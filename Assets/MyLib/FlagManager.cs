﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



//スクリプト実行順、一番最後に実行
[DefaultExecutionOrder(int.MaxValue)]
public class Flag : MonoBehaviour
{
    public enum Mode
    {
        Enter,
        Stay,
        Exit,
        Off
    }
    Mode mode;
    public Mode flagMode => mode;

    bool inputFlag;
    bool prevFlag;
    bool currentFlag;

    public void Push()
    {
        inputFlag = true;
    }

    void Update()
    {
        prevFlag = currentFlag;
        currentFlag = inputFlag;
        if (!prevFlag && !currentFlag) mode = Mode.Off;
        else if (prevFlag == currentFlag) mode = Mode.Stay;
        else if (prevFlag && !currentFlag) mode = Mode.Exit;
        else if (!prevFlag && currentFlag) mode = Mode.Enter;
    }
}
