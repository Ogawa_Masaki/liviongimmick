﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;
using System.Linq;

public class Mathaf : MonoBehaviour {
    /// <summary>
    /// 子がいないオブジェクトを削除する
    /// 
    /// </summary>
    /// <param name="par"></param>
    static public void DestroyInNotChild(Transform par)
    {
        foreach(Transform t in par)
        {
            if(t.childCount <= 0)
            {
                Destroy(t.gameObject);
            }
        }

    }
    /// <summary>
    ///名前の後に_番号をつける(BlenderのCell用）
    /// </summary>
    /// <param name="par"></param>
    static public void ZeroNumName_Blender(Transform par)
    {
        List<Transform> list = new List<Transform>();
        foreach (Transform t in par)
        {
            list.Add(t);
        }
        list = list.OrderBy(b => b.name.Length).ToList();
        foreach (Transform t in list)
        {
            string[] sprName = t.name.Split('_');
            int num = sprName.Length - 1;
            if(num >= 1)
            {
                if (!sprName[num].Contains("0"))
                {
                    t.name += "_cell";
                }
            }
        }
    }
    /// <summary>
    /// 名前をフォルダーとして解釈させ、階層構造を作る（Blenderで破片を作った時
    /// 名前が階層構造になってるけど、自動で親子関係は作ってくれないから
    /// やむをえず、こういう形で自動実装
    /// </summary>
    /// <param name="par"></param>
    static public void NameIsFolder(Transform par)
    {
        List<Transform> list = new List<Transform>();
        foreach(Transform t in par)
        {
            list.Add(t);
        }
        list = list.OrderBy(b => b.name.Length).ToList();
        foreach(Transform t in list)
        {
            NameIsFolder(ref list, t);
        }
    }
    static public void NameIsFolder(ref List<Transform> par , Transform papa)
    {
        foreach (Transform t in par)
        {
            if (t.name.Contains(papa.name) && t != papa)
            {
                t.parent = papa;
            }
        }
    }

    /// <summary>
    /// UnityのNullはC＃のNullとは違うため、場合によってはif nullを通ってしまう、多分!=とかオーバーライドされてる
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    static public bool isNull(Object obj)
    {
        return (obj != null);
    }
    /// <summary>
    /// t　から 最後のparentまで、T(何かしらのコンポーネント）を探し、あった場合はそのコンポーネントoutし、true
    /// 無ければfalse
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t"></param>
    /// <param name="res"></param>
    static public bool SearchComponentToParent<T>(Transform t , out T res)
    {
        res = default;
        do
        {
            T c = t.GetComponent<T>();
            Debug.Log(c);
            if (isNull(c as Object))
            {
                res = c;
                return true;
            }
            t = t.parent;
        } while (t != null);

        Debug.Log("SCTP Err");
        return false;
    }

    /// <summary>
    /// parから下のすべてのトランスフォームをリストにまとめて返す
    /// </summary>
    /// <param name="SetTrans"></param>
    /// <param name="par"></param>
    static public void SearchBone(ref List<Transform> SetTrans, Transform par)
    {
        foreach (Transform c in par)
        {
            SetTrans.Add(c);
            SearchBone(ref SetTrans, c);
        }
    }//parを起点として、それ以降のすべてのトランスフォームをリストに追加する



    //単位ベクトル生成
    static public Vector3 CreateUINTVector(Vector3 v)
    {
        float len = Mathf.Pow((v.x * v.x) + (v.y * v.y) + (v.z * v.z), 0.5f); //ベクトル長さ

        Vector3 ret;
        ret.x = v.x / len;
        ret.y = v.y / len;
        ret.z = v.z / len;

        return ret;
    }


    /// <summary>
    ///点Pと直線ABから線上最近点を求める
    /// </summary>
    /// <param name="fromPos"></param>
    /// <param name="toPosA"></param>
    /// <param name="toPosB"></param>
    /// <returns></returns>
    static public Vector3 NearPosOnLine(Vector3 fromPos, Vector3 toPosA, Vector3 toPosB)
    {
        Vector3 AB = toPosB - toPosA;
        Vector3 AP = fromPos - toPosA; 

        //ABの単位ベクトルを計算
        Vector3 nAB = CreateUINTVector(AB);

        //Aから線上最近点までの距離（ABベクトルの後ろにあるときはマイナス値）
        float distance =  Vector3.Dot(nAB, AP);
        distance = Mathf.Clamp(distance, 0, Vector3.Distance(toPosA,toPosB));


        //線上最近点
        return toPosA + (nAB * distance);
    }

    //IKに使うためのセット、コア部分から遠ざかる順番に設定しないとダメなの
    //先端用の計算用オブジェはかざりだから使わないでね、作れるデータは作ってあるけど角度の基準がないから、そこが設定できなかった
    public static GameObject[] IKSet(GameObject coreObj , params GameObject[] Obj )
	{
		GameObject[] cObj = new GameObject[Obj.Length];
		for (int i = 0; i < Obj.Length; i++) 
		{
			//オブジェ生成
			cObj [i] = new GameObject ("cObj" + i);
			//オブジェお親設定
			if (i == 0) {
				cObj [i].transform.parent = coreObj.transform;
			} else {
				cObj [i].transform.parent = Obj [i - 1].transform;
			}
			//オブジェの座標設定
			cObj [i].transform.position = Obj [i].transform.position;

			//オブジェの角度設定
			if (i < Obj.Length - 1) 
			{
				cObj [i].transform.LookAt (Obj [i + 1].transform.position, coreObj.transform.forward);
			}
			else 
			{
				//				Vector3 toPos = (Obj [i].transform.position-Obj [i - 1].transform.position)+Obj [i].transform.position;
				//				cObj [i].transform.LookAt (toPos, coreObj.transform.forward);
				Vector3 toPos = -coreObj.transform.forward+Obj [i].transform.position;
				cObj [i].transform.LookAt (toPos, coreObj.transform.up);
			}

			//表示用ぶじぇの親を計算用オブジェに設定
			Obj[i].transform.parent = cObj[i].transform;


//			coreObj.AddComponent<NetMotionLink> ().netId .Objs = cObj [i];

		}

		return cObj;
	}

	//
	// IK　a b c オブジェを tObjにIKする　、bオブジェの回転方向は、とりあえずaオブジェを中心とした所から山なりだけれども、他の設定もつくらないとね
	//



	public enum RadType
	{
		Not,
		Abs,
		Abs_X_Rev,
		Abs_Y_Rev,
		X_Onry,
		X_OnryRev,
		Y_Onry,
		Y_OnryRev
	}


    public static GameObject cCube = null;
    public static void IK(GameObject aObj , GameObject bObj , GameObject cObj , GameObject tObj , float RotPsc , RadType RT = RadType.Not , GameObject pObj = null)
	{
		if (cCube == null) {cCube = new GameObject ("calCube");}
		
		aObj.transform.localEulerAngles = new Vector3(0f,0f,0f);
		bObj.transform.localEulerAngles = new Vector3 (0f, 0f, 0f);

		float atLen = Vector3.Distance (aObj.transform.position, tObj.transform.position);
		float abLen = Vector3.Distance (aObj.transform.position, bObj.transform.position);
		float bcLen = Vector3.Distance (bObj.transform.position, cObj.transform.position);


		float calic = (((abLen * abLen) + (bcLen * bcLen)) - (atLen*atLen))/(2*abLen*bcLen);



		float lenRad = Mathf.Acos (calic);

		if (lenRad > 0f) 
		{
			float bRadian = (Mathf.PI - lenRad) * Mathf.Rad2Deg;

			Vector3 atOfs = aObj.transform.position - tObj.transform.position;

			atOfs = Quaternion.Inverse(aObj.transform.parent.rotation) * atOfs;

			float atLenx = atOfs.x;
			float atLenz = atOfs.z;
			float mmLen = Mathf.Abs(atLenx)+Mathf.Abs(atLenz);

			switch (RT) {
			case RadType.Not:
				{
					atLenx /= mmLen;
					atLenz /= mmLen;
				}
				break;
			case RadType.Abs:
				{
					atLenx /= mmLen;
					atLenz /= mmLen;
					atLenx = Mathf.Abs (atLenx);
					atLenz = Mathf.Abs (atLenz);
				}
				break;
			case RadType.Abs_X_Rev:
				{
					atLenx /= mmLen;
					atLenz /= mmLen;
					atLenx = Mathf.Abs (atLenx);
					atLenz = -Mathf.Abs (atLenz);
				}
				break;
			case RadType.Abs_Y_Rev:
				{
					atLenx /= mmLen;
					atLenz /= mmLen;
					atLenx = -Mathf.Abs (atLenx);
					atLenz = Mathf.Abs (atLenz);
				}
				break;
			case RadType.X_Onry:
				{
					atLenz = 1f;
					atLenx = 0f;
				}
				break;
			case RadType.X_OnryRev:
				{
					atLenz = -1f;
					atLenx = 0f;
				}
				break;
			case RadType.Y_Onry:
				{
					atLenz = 0f;
					atLenx = 1f;
				}
				break;
			case RadType.Y_OnryRev:
				{
					atLenz = 0f;
					atLenx = -1f;
				}
				break;
			}

			bObj.transform.localEulerAngles = new Vector3 (bRadian*atLenz*RotPsc,bRadian*atLenx*RotPsc, 0f);

		}

		Transform parObj = aObj.transform.parent;

		Transform calpObj = parObj;
		if (pObj != null) 
		{
			calpObj = pObj.transform;
		}

		cCube.transform.position = aObj.transform.position;
		cCube.transform.eulerAngles = aObj.transform.eulerAngles;
		cCube.transform.LookAt (cObj.transform.position, calpObj.up);

		aObj.transform.parent = cCube.transform;
		cCube.transform.LookAt (tObj.transform.position, calpObj.forward);

		aObj.transform.parent = parObj;

	}

    public static Quaternion NormalizeQuater(Quaternion rad , Quaternion ofs)
    {
        Vector3 r = rad.eulerAngles;
        Vector3 o = ofs.eulerAngles;

        r -= o;
        NormalizeRads(r);
        r += o;
        return Quaternion.Euler(r);
    }

	public static float NormalizeRad(float rad , float num = 180f)
	{
		float hn = num * 0.5f;
		if(rad >  num){rad-=360;}
		if(rad < -num){rad+=360;}

		if(rad >  num){NormalizeRad (rad);}
		if(rad < -num){NormalizeRad(rad);}

		return rad;
	}
    public static Vector3 NormalizeRads(Vector3 Rads, float num = 180f)
    {
        Rads.x = NormalizeRad(Rads.x, num);
        Rads.y = NormalizeRad(Rads.y, num);
        Rads.z = NormalizeRad(Rads.z, num);
        return Rads;
    }
    public static Vector3 NormalizeRads(Vector3 Rads, Vector3 nums)
    {
        Rads.x = NormalizeRad(Rads.x, nums.x);
        Rads.y = NormalizeRad(Rads.y, nums.y);
        Rads.z = NormalizeRad(Rads.z, nums.z);
        return Rads;
    }


    public static float toRad(float angle)
	{
		return angle * 180 / Mathf.PI;
	}

	public static bool Fit(ref float fromNum , float toNum ,float speed)
	{
		bool fitFlag = true;
		if (fromNum > toNum) {fitFlag = false;fromNum -= speed;if (fromNum < toNum) {fromNum = toNum;}}
		if (fromNum < toNum) {fitFlag = false;fromNum += speed;if (fromNum > toNum) {fromNum = toNum;}}

		return fitFlag;
	}

	public static bool Fit(ref Vector3 pos , Vector3 toPos ,float speed)
	{
		bool fitFlag = true;
		if (pos.x > toPos.x) {fitFlag = false;pos.x -= speed;if (pos.x < toPos.x) {pos.x = toPos.x;}}
		if (pos.x < toPos.x) {fitFlag = false;pos.x += speed;if (pos.x > toPos.x) {pos.x = toPos.x;}}
		if (pos.y > toPos.y) {fitFlag = false;pos.y -= speed;if (pos.y < toPos.y) {pos.y = toPos.y;}}
		if (pos.y < toPos.y) {fitFlag = false;pos.y += speed;if (pos.y > toPos.y) {pos.y = toPos.y;}}
		if (pos.z > toPos.z) {fitFlag = false;pos.z -= speed;if (pos.z < toPos.z) {pos.z = toPos.z;}}
		if (pos.z < toPos.z) {fitFlag = false;pos.z += speed;if (pos.z > toPos.z) {pos.z = toPos.z;}}

		return fitFlag;
	}
	public static bool Fit(ref Vector3 pos , Vector3 toPos ,float speed , float min , float max)
	{
		float xspeed = Mathf.SmoothStep (min, max, pos.x)*speed;
		float yspeed = Mathf.SmoothStep (min, max, pos.y)*speed;
		float zspeed = Mathf.SmoothStep (min, max, pos.z)*speed;
		bool fitFlag = true;
		if (pos.x > toPos.x) {fitFlag = false;pos.x -= xspeed;if (pos.x < toPos.x) {pos.x = toPos.x;}}
		if (pos.x < toPos.x) {fitFlag = false;pos.x += xspeed;if (pos.x > toPos.x) {pos.x = toPos.x;}}
		if (pos.y > toPos.y) {fitFlag = false;pos.y -= yspeed;if (pos.y < toPos.y) {pos.y = toPos.y;}}
		if (pos.y < toPos.y) {fitFlag = false;pos.y += yspeed;if (pos.y > toPos.y) {pos.y = toPos.y;}}
		if (pos.z > toPos.z) {fitFlag = false;pos.z -= zspeed;if (pos.z < toPos.z) {pos.z = toPos.z;}}
		if (pos.z < toPos.z) {fitFlag = false;pos.z += zspeed;if (pos.z > toPos.z) {pos.z = toPos.z;}}

		return fitFlag;
	}
	public static bool FitZero(ref Vector3 pos , float speed)
	{
		bool fitFlag = true;
		if (pos.x > 0f) {fitFlag = false;pos.x -= speed;if (pos.x < 0f) {pos.x = 0f;}}
		if (pos.x < 0f) {fitFlag = false;pos.x += speed;if (pos.x > 0f) {pos.x = 0f;}}
		if (pos.y > 0f) {fitFlag = false;pos.y -= speed;if (pos.y < 0f) {pos.y = 0f;}}
		if (pos.y < 0f) {fitFlag = false;pos.y += speed;if (pos.y > 0f) {pos.y = 0f;}}
		if (pos.z > 0f) {fitFlag = false;pos.z -= speed;if (pos.z < 0f) {pos.z = 0f;}}
		if (pos.z < 0f) {fitFlag = false;pos.z += speed;if (pos.z > 0f) {pos.z = 0f;}}

		return fitFlag;
	}

	public static void DestroyAllChil(GameObject g)
	{
		foreach (Transform n in g.transform) 
		{
			Destroy (n.gameObject);
		}
	}


	//テクスチャを単に書き換えると、それを使っている他のマテリアルにまで影響を与えてしまう
	//故一度テクスチャを複製して、それを使用テクスチャとすれば、それをいくら書き換えても他に影響はない
	public enum HookMode
	{
		MAIN, 
		SUB1,
		SUB2,
		SUB3,
		SUB4,
		SUB5
	}
	public static Texture2D getTexture(Renderer targetRenderer , HookMode hm)
	{
		Texture2D masterTexture;
		switch (hm) {
		default:{masterTexture = (Texture2D)targetRenderer.material.mainTexture;}break;//case MAIN
		case HookMode.SUB1:{masterTexture = (Texture2D)targetRenderer.material.mainTexture;}break;
		case HookMode.SUB2:{masterTexture = (Texture2D)targetRenderer.material.mainTexture;}break;
		case HookMode.SUB3:{masterTexture = (Texture2D)targetRenderer.material.mainTexture;}break;
		case HookMode.SUB4:{masterTexture = (Texture2D)targetRenderer.material.mainTexture;}break;
		case HookMode.SUB5:{masterTexture = (Texture2D)targetRenderer.material.mainTexture;}break;
		}
		return masterTexture;
	}


    /// <summary>
    /// テクスチャの差し替え（現在はどのHookModeを選んでもメインのみ）
    /// </summary>
    /// <param name="targetRenderer"></param>
    /// <param name="hm"></param>
    /// <param name="newTexture"></param>
	public static void setTexture(ref Renderer targetRenderer , HookMode hm , Texture2D newTexture)
	{
		//レンダラ-が使うテクスチャを設定
		switch (hm) {
		default:{targetRenderer.material.mainTexture = newTexture;}break;//case MAIN
		case HookMode.SUB1:{targetRenderer.material.mainTexture = newTexture;}break;
		case HookMode.SUB2:{targetRenderer.material.mainTexture = newTexture;}break;
		case HookMode.SUB3:{targetRenderer.material.mainTexture = newTexture;}break;
		case HookMode.SUB4:{targetRenderer.material.mainTexture = newTexture;}break;
		case HookMode.SUB5:{targetRenderer.material.mainTexture = newTexture;}break;
		}
	}


    /// <summary>
    /// targetのレンダラーに使われているテクスチャを複製する（今後直接書き込むとかする時のため）
    /// </summary>
    /// <param name="target"></param>
    /// <param name="hm"></param>
    /// <returns></returns>
	public static bool textureHook(GameObject target , HookMode hm)
	{
		//レンダラを取得
		Renderer targetRenderer = target.GetComponent<Renderer> ();
		if (!targetRenderer) {
			return false;
		}

		//元てくすちゃを取得
		Texture2D masterTexture = getTexture(targetRenderer , hm);
		if (masterTexture) {
			return false;
		}



        //ピクセルの先頭を取得（配列の頭）
        Color[] pTex = masterTexture.GetPixels ();

        //空の編集用テクスチャを生成
        Color[] editTexture = new Color[pTex.Length];

        //編集用テクスチャに元テクスチャをコピー
        for (int i = 0; i < pTex.Length; i++)
        {
			editTexture.SetValue (pTex[i], i);
		}

		//テクスチャマネージャを生成
		Texture2D newTexture = new Texture2D (masterTexture.width, masterTexture.height, TextureFormat.RGBA32, false);
		//マネーじゃのステータスを設定
		newTexture.filterMode = FilterMode.Point;
		newTexture.SetPixels (editTexture);		//用意いたテクスチャを設定！
		newTexture.Apply ();//てくすちゃ完成！



		setTexture (ref targetRenderer, hm, newTexture);
			

		return true;
	}

    /// <summary>
    /// 対象のテクスチャに書き込む
    /// </summary>
    /// <param name="target"></param>
    /// <param name="hm"></param>
    /// <param name="stunpTexture"></param>
    /// <param name="u"></param>
    /// <param name="v"></param>
	public static void stunpTexture(GameObject target , HookMode hm , Texture2D stunpTexture , int u , int v)
	{
		//レンダラを取得
		Renderer targetRenderer = target.GetComponent<Renderer> ();
		if (!targetRenderer) {
			return;
		}

		//元てくすちゃを取得
		Texture2D masterTexture = getTexture(targetRenderer , hm);
		if (masterTexture) {
			return;
		}


		masterTexture.SetPixels (u,v, stunpTexture.width, stunpTexture.height, stunpTexture.GetPixels());



	}

    /// <summary>
    /// aとbの値の変化の間にVautLineが含まれているか否か（一定のラインを超えた瞬間）
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <param name="VautLine"></param>
    /// <returns></returns>
	public static bool numVaut(float a, float b , float VautLine)
	{
		if ((a > VautLine && b <= VautLine) || (a <= VautLine && b > VautLine)) {
			return true;
		}

		return false;
		
	}



	public static Vector3 LerpEuler(Transform a , Transform b , float len)
	{

		Vector3 ae = a.eulerAngles;

		float ang = Quaternion.Angle (a.rotation, b.rotation);
		Quaternion abQ = Quaternion.RotateTowards(a.rotation,b.rotation,ang*len);

		ae += NormalizeRads (abQ.eulerAngles - ae);



		return ae; 
	}
}
