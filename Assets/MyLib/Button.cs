﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



//スクリプト実行順、一番最後に実行
[DefaultExecutionOrder(int.MaxValue)]
public class Button : MonoBehaviour
{

    public enum Mode
    {
        Enter,
        Stay,
        Exit,
        Off
    }
    Mode mode;


    bool currentFlag;
    bool prevFlag;
    bool isOn;
    private void FixedUpdate()
    {
    }

    void Update()
    {
        prevFlag = isOn;
        isOn = currentFlag;

        if (!prevFlag && !currentFlag) mode = Mode.Off;
        else if (prevFlag == currentFlag) mode = Mode.Stay;
        else if (prevFlag && !currentFlag) mode = Mode.Exit;
        else if (!prevFlag && currentFlag) mode = Mode.Enter;
    }
}
