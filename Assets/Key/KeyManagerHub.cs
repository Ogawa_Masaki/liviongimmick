﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public enum Key_Hub
{
    U, R, D, L,
    L1, L2, L3,
    X, A, B, Y,
    R1, R2, R3,
    Option,
    Select,
    Home,
    Share
}

[RequireComponent(typeof(KeyManager_Switch))]
[RequireComponent(typeof(JoyconManager))]
[RequireComponent(typeof(KeyManager_PS4))]
[RequireComponent(typeof(KeyManager_PC))]

public class KeyManagerHub : MonoBehaviour
{

    public enum HardMode
    {
        PS4,
        Switch,
        PC
    }

    KeyManager_PS4 KM_PS;
    KeyManager_Switch KM_SW;
    KeyManager_PC KM_PC;

    public HardMode hard;

    public bool LRRevFlag = false;//⇔スティックを入れ替え

    bool Enter_PS4(Key_Hub k)
    {
        switch (k)
        {
            case Key_Hub.U: { return KM_PS.enterUP; }
            case Key_Hub.R: { return KM_PS.enterRIGHT; }
            case Key_Hub.D: { return KM_PS.enterDOWN; }
            case Key_Hub.L: { return KM_PS.enterLEFT; }

            case Key_Hub.X: { return KM_PS.enterSANKAKU; }
            case Key_Hub.A: { return KM_PS.enterMARU; }
            case Key_Hub.B: { return KM_PS.enterBATU; }
            case Key_Hub.Y: { return KM_PS.enterSIKAKU; }

            case Key_Hub.L1: { return KM_PS.enterL1; }
            case Key_Hub.L2: { return KM_PS.enterL2; }
            case Key_Hub.L3: { return KM_PS.enterL3; }

            case Key_Hub.R1: { return KM_PS.enterR1; }
            case Key_Hub.R2: { return KM_PS.enterR2; }
            case Key_Hub.R3: { return KM_PS.enterR3; }

            case Key_Hub.Option: { return KM_PS.enterOPTION; }
            case Key_Hub.Select: { return KM_PS.enterPAD; }
            case Key_Hub.Home: { return KM_PS.enterPS; }
            case Key_Hub.Share: { return KM_PS.enterSHARE; }
        }
        return false;
    }

    public void Vibrate(bool lFlag,bool rFlag)
    {
        switch (hard)
        {
            case HardMode.PS4:
                {
                    //                    KM_PS.Vibrate(lFlag, rFlag);
                }
                break;
            case HardMode.Switch:
                {
                    KM_SW.Vibrate(lFlag,rFlag);
                }
                break;
            case HardMode.PC:
                {

                }
                break;
        }

    }

    public Transform D2Trans = null;
    public Vector3 GetStickVectorFromCamera(out float stickLen, bool isLeft, float minOfs = 0.01f, float maxOfs = 0.99f)
    {
        //キー入力情報を取得
        Vector3 vel = GetStickVel(isLeft);
        stickLen = Mathf.Clamp(vel.magnitude, 0f, 1f);
        if (stickLen <= minOfs) stickLen = 0f;
        if (stickLen >= maxOfs) stickLen = 1f;

        Vector3 stickVel = Vector3.zero;
        if (stickLen > 0f)
        {
            //操作ベクトル計算
            Vector3 cfVel = Camera.main.transform.forward;
            Vector3 crVel = Camera.main.transform.right;
            stickVel = ((crVel * vel.x) + (cfVel * vel.y));
            stickVel = stickVel.normalized;
            stickVel = Quaternion.LookRotation(stickVel) * Vector3.forward * stickLen;

            if (D2Trans)
            {
                stickVel = Vector3.Dot(stickVel, D2Trans.right) * D2Trans.right;
            }
        }

        return stickVel;
    }
    bool Stay_PS4(Key_Hub k)
    {
        switch (k)
        {
            case Key_Hub.U: { return KM_PS.stayUP; }
            case Key_Hub.R: { return KM_PS.stayRIGHT; }
            case Key_Hub.D: { return KM_PS.stayDOWN; }
            case Key_Hub.L: { return KM_PS.stayLEFT; }

            case Key_Hub.X: { return KM_PS.staySANKAKU; }
            case Key_Hub.A: { return KM_PS.stayMARU; }
            case Key_Hub.B: { return KM_PS.stayBATU; }
            case Key_Hub.Y: { return KM_PS.staySIKAKU; }

            case Key_Hub.L1: { return KM_PS.stayL1; }
            case Key_Hub.L2: { return KM_PS.stayL2; }
            case Key_Hub.L3: { return KM_PS.stayL3; }

            case Key_Hub.R1: { return KM_PS.stayR1; }
            case Key_Hub.R2: { return KM_PS.stayR2; }
            case Key_Hub.R3: { return KM_PS.stayR3; }

            case Key_Hub.Option: { return KM_PS.stayOPTION; }
            case Key_Hub.Select: { return KM_PS.stayPAD; }
            case Key_Hub.Home: { return KM_PS.stayPS; }
            case Key_Hub.Share: { return KM_PS.staySHARE; }
        }
        return false;
    }
    bool Exit_PS4(Key_Hub k)
    {
        switch (k)
        {
            case Key_Hub.U: { return KM_PS.exitUP; }
            case Key_Hub.R: { return KM_PS.exitRIGHT; }
            case Key_Hub.D: { return KM_PS.exitDOWN; }
            case Key_Hub.L: { return KM_PS.exitLEFT; }

            case Key_Hub.X: { return KM_PS.exitSANKAKU; }
            case Key_Hub.A: { return KM_PS.exitMARU; }
            case Key_Hub.B: { return KM_PS.exitBATU; }
            case Key_Hub.Y: { return KM_PS.exitSIKAKU; }

            case Key_Hub.L1: { return KM_PS.exitL1; }
            case Key_Hub.L2: { return KM_PS.exitL2; }
            case Key_Hub.L3: { return KM_PS.exitL3; }

            case Key_Hub.R1: { return KM_PS.exitR1; }
            case Key_Hub.R2: { return KM_PS.exitR2; }
            case Key_Hub.R3: { return KM_PS.exitR3; }

            case Key_Hub.Option: { return KM_PS.exitOPTION; }
            case Key_Hub.Select: { return KM_PS.exitPAD; }
            case Key_Hub.Home: { return KM_PS.exitPS; }
            case Key_Hub.Share: { return KM_PS.exitSHARE; }
        }
        return false;
    }


    bool SwitchConvert(Key_Hub k,ref Joycon.Button button,ref bool isLeft)
    {
        button = Joycon.Button.HOME;
        isLeft = false;
        switch (k)
        {
            case Key_Hub.U: { button = Joycon.Button.DPAD_UP; isLeft = true; } break;
            case Key_Hub.R: { button = Joycon.Button.DPAD_RIGHT; isLeft = true; } break;
            case Key_Hub.D: { button = Joycon.Button.DPAD_DOWN; isLeft = true; } break;
            case Key_Hub.L: { button = Joycon.Button.DPAD_LEFT; isLeft = true; } break;

            case Key_Hub.X: { button = Joycon.Button.DPAD_UP; isLeft = false; } break;
            case Key_Hub.A: { button = Joycon.Button.DPAD_RIGHT; isLeft = false; } break;
            case Key_Hub.B: { button = Joycon.Button.DPAD_DOWN; isLeft = false; } break;
            case Key_Hub.Y: { button = Joycon.Button.DPAD_LEFT; isLeft = false; } break;

            case Key_Hub.L1: { button = Joycon.Button.SHOULDER_1; isLeft = true; } break;
            case Key_Hub.L2: { button = Joycon.Button.SHOULDER_2; isLeft = true; } break;
            case Key_Hub.L3: { button = Joycon.Button.STICK; isLeft = true; } break;

            case Key_Hub.R1: { button = Joycon.Button.SHOULDER_1; isLeft = false; } break;
            case Key_Hub.R2: { button = Joycon.Button.SHOULDER_2; isLeft = false; } break;
            case Key_Hub.R3: { button = Joycon.Button.STICK; isLeft = false; } break;

            case Key_Hub.Option: { button = Joycon.Button.PLUS; isLeft = false; } break;
            case Key_Hub.Select: { button = Joycon.Button.MINUS; isLeft = true; } break;
            case Key_Hub.Home: { button = Joycon.Button.HOME; isLeft = false; } break;
            case Key_Hub.Share: { button = Joycon.Button.CAPTURE; isLeft = true; } break;
            default: { return false; }
        }
        return true;
    }
    bool Enter_Switch(Key_Hub k)
    {
        Joycon.Button button = Joycon.Button.HOME;
        bool isLeft = false;
        SwitchConvert(k,ref button,ref isLeft);
        return KM_SW.GetKeyDown(button,isLeft);
    }
    bool Stay_Switch(Key_Hub k)
    {
        Joycon.Button button = Joycon.Button.HOME;
        bool isLeft = false;
        SwitchConvert(k,ref button,ref isLeft);
        return KM_SW.GetKeyStay(button,isLeft);
    }
    bool Exit_Switch(Key_Hub k)
    {
        Joycon.Button button = Joycon.Button.HOME;
        bool isLeft = false;
        SwitchConvert(k,ref button,ref isLeft);
        return KM_SW.GetKeyUp(button,isLeft);
    }

    bool Enter_PC(Key_Hub k)
    {
        switch (k)
        {
            case Key_Hub.U: { return Input.GetKeyDown(KeyCode.UpArrow); }
            case Key_Hub.R: { return Input.GetKeyDown(KeyCode.RightArrow); }
            case Key_Hub.D: { return Input.GetKeyDown(KeyCode.DownArrow); }
            case Key_Hub.L: { return Input.GetKeyDown(KeyCode.LeftArrow); }

            case Key_Hub.X: { return Input.GetKeyDown(KeyCode.LeftShift); }
            case Key_Hub.A: { return Input.GetKeyDown(KeyCode.Return); }
            case Key_Hub.B: { return Input.GetKeyDown(KeyCode.Space); }
            case Key_Hub.Y: { return Input.GetKeyDown(KeyCode.Tab); }

            case Key_Hub.L1: { return Input.GetMouseButtonDown(2); }
            case Key_Hub.L2: { return Input.GetMouseButtonDown(1); }
            case Key_Hub.L3: { return Input.GetKeyDown(KeyCode.P); }

            case Key_Hub.R1: { return Input.GetMouseButtonDown(0); }
            case Key_Hub.R2: { return Input.GetKeyDown(KeyCode.J); }
            case Key_Hub.R3: { return Input.GetKeyDown(KeyCode.Q); }

            case Key_Hub.Option: { return Input.GetKeyDown(KeyCode.Escape); }
            case Key_Hub.Select: { return Input.GetKeyDown(KeyCode.Alpha1); }
            case Key_Hub.Home: { return Input.GetKeyDown(KeyCode.Alpha2); }
            case Key_Hub.Share: { return Input.GetKeyDown(KeyCode.Alpha3); }
        }
        return false;
    }
    bool Stay_PC(Key_Hub k)
    {
        switch (k)
        {
            case Key_Hub.U: { return Input.GetKey(KeyCode.UpArrow); }
            case Key_Hub.R: { return Input.GetKey(KeyCode.RightArrow); }
            case Key_Hub.D: { return Input.GetKey(KeyCode.DownArrow); }
            case Key_Hub.L: { return Input.GetKey(KeyCode.LeftArrow); }

            case Key_Hub.X: { return Input.GetKey(KeyCode.LeftShift); }
            case Key_Hub.A: { return Input.GetKey(KeyCode.Return); }
            case Key_Hub.B: { return Input.GetKey(KeyCode.Space); }
            case Key_Hub.Y: { return Input.GetKey(KeyCode.Tab); }

            case Key_Hub.L1: { return Input.GetMouseButton(2); }
            case Key_Hub.L2: { return Input.GetMouseButton(1); }
            case Key_Hub.L3: { return Input.GetKey(KeyCode.P); }

            case Key_Hub.R1: { return Input.GetMouseButton(0); }
            case Key_Hub.R2: { return Input.GetKey(KeyCode.J); }
            case Key_Hub.R3: { return Input.GetKey(KeyCode.Q); }

            case Key_Hub.Option: { return Input.GetKey(KeyCode.Escape); }
            case Key_Hub.Select: { return Input.GetKey(KeyCode.Alpha1); }
            case Key_Hub.Home: { return Input.GetKey(KeyCode.Alpha2); }
            case Key_Hub.Share: { return Input.GetKey(KeyCode.Alpha3); }
        }
        return false;
    }
    bool Exit_PC(Key_Hub k)
    {
        switch (k)
        {
            case Key_Hub.U: { return Input.GetKeyUp(KeyCode.UpArrow); }
            case Key_Hub.R: { return Input.GetKeyUp(KeyCode.RightArrow); }
            case Key_Hub.D: { return Input.GetKeyUp(KeyCode.DownArrow); }
            case Key_Hub.L: { return Input.GetKeyUp(KeyCode.LeftArrow); }

            case Key_Hub.X: { return Input.GetKeyUp(KeyCode.LeftShift); }
            case Key_Hub.A: { return Input.GetKeyUp(KeyCode.Return); }
            case Key_Hub.B: { return Input.GetKeyUp(KeyCode.Space); }
            case Key_Hub.Y: { return Input.GetKeyUp(KeyCode.Tab); }

            case Key_Hub.L1: { return Input.GetMouseButtonUp(2); }
            case Key_Hub.L2: { return Input.GetMouseButtonUp(1); }
            case Key_Hub.L3: { return Input.GetKeyUp(KeyCode.P); }

            case Key_Hub.R1: { return Input.GetMouseButtonUp(0); }
            case Key_Hub.R2: { return Input.GetKeyUp(KeyCode.J); }
            case Key_Hub.R3: { return Input.GetKeyUp(KeyCode.Q); }

            case Key_Hub.Option: { return Input.GetKeyUp(KeyCode.Escape); }
            case Key_Hub.Select: { return Input.GetKeyUp(KeyCode.Alpha1); }
            case Key_Hub.Home: { return Input.GetKeyUp(KeyCode.Alpha2); }
            case Key_Hub.Share: { return Input.GetKeyUp(KeyCode.Alpha3); }
        }
        return false;
    }

    bool GetEnter(Key_Hub k,HardMode h)
    {
        bool flag = false;
        switch (h)
        {
            case HardMode.PS4: { flag = Enter_PS4(k); } break;
            case HardMode.Switch: { flag = Enter_Switch(k); } break;
            case HardMode.PC: { flag = Enter_PC(k); } break;
        }

        return flag;
    }
    bool GetStay(Key_Hub k,HardMode h)
    {
        bool flag = false;
        switch (h)
        {
            case HardMode.PS4: { flag = Stay_PS4(k); } break;
            case HardMode.Switch: { flag = Stay_Switch(k); } break;
            case HardMode.PC: { flag = Stay_PC(k); } break;
        }

        return flag;

    }
    bool GetExit(Key_Hub k,HardMode h)
    {
        bool flag = false;
        switch (h)
        {
            case HardMode.PS4: { flag = Exit_PS4(k); } break;
            case HardMode.Switch: { flag = Exit_Switch(k); } break;
            case HardMode.PC: { flag = Exit_PC(k); } break;
        }

        return flag;

    }


    public bool GetAnyKey()
    {
        for (int i = 0; i < Enum.GetNames(typeof(Flag.Mode)).Length; i++)
        {
            for (int j = 0; j < Enum.GetNames(typeof(Key_Hub)).Length; j++)
            {
                if (GetKey((Flag.Mode)i,(Key_Hub)j))
                {
                    return true;
                }
            }
        }
        return false;
    }
    public bool GetKey(Flag.Mode km,Key_Hub key)
    {
        bool flag = false;
        switch (km)
        {
            case Flag.Mode.Enter:
                {
                    flag = GetEnter(key,hard);
                }
                break;
            case Flag.Mode.Stay:
                {
                    flag = GetStay(key,hard);
                }
                break;
            case Flag.Mode.Exit:
                {
                    flag = GetExit(key,hard);
                }
                break;
        }

        return flag;
    }

    public Vector3 GetStickVel(bool isLeft)
    {
        if (LRRevFlag) { isLeft = !isLeft; }
        Vector3 vel = new Vector3(0f,0f,0f);
        switch (hard)
        {
            case HardMode.PS4:
                {
                    if (isLeft)
                    {
                        vel = KM_PS.LStickVelLocal;
                        float tmp = vel.y;
                        vel.y = -vel.z;
                        vel.z = tmp;
                    }
                    else
                    {
                        vel = KM_PS.RStickVelLocal;
                        float tmp = vel.y;
                        vel.y = vel.z;
                        vel.z = tmp;
                    }
                }
                break;
            case HardMode.Switch:
                {
                    if (isLeft)
                    {
                        vel = KM_SW.LKey.JoyStick;
                    }
                    else
                    {
                        vel = KM_SW.RKey.JoyStick;
                    }
                }
                break;
            case HardMode.PC:
                {
                    vel = KM_PC.GetStickVel(isLeft);
                }
                break;
        }

        return vel;
    }
    public Vector3 GetStickVectorFromNormal(bool isLeft, Vector3 nor, bool revFlag)
    {
        Vector3 stickVel = -GetStickVel(isLeft);
        float stickLen = stickVel.magnitude;
        stickVel.z = stickVel.y;
        stickVel.y = 0f;

        Vector3 f = Camera.main.transform.forward;


        float camNorAngle = Vector3.Angle(Camera.main.transform.forward, nor);
        float camNorDot = Vector3.Dot(nor, Camera.main.transform.forward);

        bool fRevFlag = camNorDot < 0f;//camNorAngle > (360f - 45f);

        bool backFlag = camNorAngle < 45f;

        if (backFlag && !fRevFlag)
        {
            stickVel.z = -stickVel.z;
        }
        if (revFlag)
        {
            f = Camera.main.transform.forward;
            f.y = Mathf.Abs(f.y);

        }
        else
        {
            if(nor.y < 0f && fRevFlag )
            {
                f = -f;
            }
        }
        f.y += Mathf.Abs(camNorDot);
        //        if(f.y < 0f)Vector3.Reflect(, Vector3.up);

        Vector3 r = Quaternion.LookRotation(f, nor) * Vector3.right;

        if (nor.y < 0f)
        {
            if (revFlag)
            {
                stickVel.x = -stickVel.x;
            }
        }
        

          Vector3 norVel = Vector3.ProjectOnPlane(((f.normalized * stickVel.z) + (r.normalized * stickVel.x)), nor);
        return norVel.normalized * stickLen;
    }
    public Vector3 GetGyroVel(bool isLeft)
    {
        Vector3 vel = new Vector3(0f,0f,0f);
        switch (hard)
        {
            case HardMode.PS4:
                {
                    if (isLeft) {/* vel = KM_PS.LStickVelLocal;*/ }
                    else
                    {/*vel = KM_PS.RStickVelLocal;*/}
                }
                break;
            case HardMode.Switch:
                {
                    if (isLeft) { vel = KM_SW.LKey.JoyGyro; }
                    else { vel = KM_SW.RKey.JoyGyro; }
                }
                break;
            case HardMode.PC:
                {
                }
                break;
        }

        return vel;
    }

    // Use this for initialization
    public void Init()
    {
        KM_PS = GetComponent<KeyManager_PS4>();
        KM_SW = GetComponent<KeyManager_Switch>();
        KM_PC = GetComponent<KeyManager_PC>();

    }

    // Update is called once per frame
    void Update()
    {

    }
}
