﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimCursol : MonoBehaviour
{
    [SerializeField]
    Transform camToCursol;
    RaycastHit camToHit;
    [SerializeField]
    Transform thisToCursol;
    RaycastHit thisToHit;
    [SerializeField]
    LayerMask mask;


    const float maxRange = 100f;

    // Start is called before the first frame update
    void Start()
    {
    }
    //昇順の表示
    public void DrawAimView(out RaycastHit resCamTo,out RaycastHit resThisTo)
    {
        resCamTo = camToHit;
        resThisTo = thisToHit;
        return;
    }

    private void Update()
    {
        var center = new Vector3(Screen.width / 2,Screen.height / 2);
        var Ray = Camera.main.ScreenPointToRay(center);
        camToHit.point = Ray.origin + (Ray.direction * maxRange);
        if (!Physics.Linecast(transform.position,camToHit.point,out thisToHit,mask))
        {
            thisToHit = camToHit;
        }


        camToCursol.position = camToHit.point;
        thisToCursol.position = thisToHit.point;

        camToCursol.rotation = Camera.main.transform.rotation;
        thisToCursol.rotation = Camera.main.transform.rotation;
    }
}
