﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder(int.MaxValue)]
public class MotionAnimationManager : MonoBehaviour
{
    [SerializeField, Range(0f,1f)]
    float squatWeight;
    //しゃがみ状態と立ち状態
    [SerializeField]
    MinMaxRange squatRange;


    [SerializeField]
    LayerMask mask;
    [SerializeField]
    Animator animator;
    [SerializeField]
    Avatar avatar;

    //本体のリジッドボディ
    [SerializeField]
    PlayerBall target;

    [SerializeField]
    float footWidth;

    //腰オブジェ
    [SerializeField]
    Transform waist;
    //腰を中心とした回転用オブジェクト
    Transform centerObj;
    //メイントランスフォームのデフォルト座標
    Vector3 defaPos;

    //ターゲットの接地座標
    Vector3 targetPos => target.Position - (target.CurrentCollisionNormal * heightOffset);

    //本体のリジッドボディの現在速度
    Vector3 currentVector => target.RB.velocity;
    Vector3 Stride => Vector3.ClampMagnitude(target.RB.velocity,1f) * 1.5f;
    float currentSpeed => target.RB.velocity.magnitude;

    //しゃがみ具合　0伏せ　１直立
    float height
    {
        get
        {
            return height;
        }
        set
        {
            height = Mathf.Clamp(value,0f,1f);
        }
    }
    //前フレームでの回転値
    Quaternion prevRot;
    //前回フレームと今回フレームのオフセット


    //本体の半径
    float heightOffset => target.Col.radius;

    //移動量タイマー
    float moveRange;

    Transform LFPos;
    Transform RFPos;
    Transform LHPos;
    Transform RHPos;

    Vector3 LFHPos;
    Vector3 RFHPos;

    float LFRWeight;
    float RFRWeight;

    float footMoveRange = 0.3f;
    Vector3 Left => -transform.right;
    Vector3 Right => transform.right;


    // Start is called before the first frame update
    void Start()
    {
        LFPos = new GameObject("LFPos").transform;
        RFPos = new GameObject("RFPos").transform;
        LHPos = new GameObject("LHPos").transform;
        RHPos = new GameObject("RHPos").transform;



        centerObj = new GameObject("wirestObj").transform;
        centerObj.position = waist.position;
        centerObj.rotation = Quaternion.LookRotation(transform.forward);
        transform.parent = centerObj;

        defaPos = transform.localPosition;


        //Lateコルーチン開始
        StartCoroutine(LateFixedUpdate());
    }
    private void OnAnimatorIK()
    {

        animator.SetIKHintPositionWeight(AvatarIKHint.LeftKnee,1f);
        animator.SetIKHintPositionWeight(AvatarIKHint.RightKnee,1f);

        //animator.SetIKRotationWeight(AvatarIKGoal.LeftFoot, LFRWeight);
        //animator.SetIKRotationWeight(AvatarIKGoal.RightFoot, RFRWeight);

        animator.SetIKPositionWeight(AvatarIKGoal.LeftFoot,1f);
        animator.SetIKPositionWeight(AvatarIKGoal.RightFoot,1f);

        animator.SetLookAtWeight(1f,0.5f,0.8f,1f,0.5f);

        animator.SetIKHintPosition(AvatarIKHint.LeftKnee,LFHPos);
        animator.SetIKHintPosition(AvatarIKHint.RightKnee,RFHPos);

        animator.SetIKRotation(AvatarIKGoal.LeftFoot, LFPos.rotation);
        animator.SetIKRotation(AvatarIKGoal.RightFoot, RFPos.rotation);

        animator.SetIKPosition(AvatarIKGoal.LeftFoot,LFPos.position);
        animator.SetIKPosition(AvatarIKGoal.RightFoot,RFPos.position);


        animator.SetLookAtPosition(target.AimPos);




    }


    Vector3 LineHit(Vector3 sta,Vector3 end)
    {
        if (Physics.Linecast(sta,end,out RaycastHit hit,mask))
        {
            return hit.point;
        }

        return end;
    }

    float footTimer;
    bool moveFootIsLeft;
    Vector3 startFootPos;
    bool MoveFoot(bool isLeft,ref float timer)
    {
        float currentRotSpeed = Vector3.Angle(transform.forward, prevRot * Vector3.forward) * 0.75f;

        timer += (currentSpeed * 0.01f) + (Time.deltaTime * (1f + (squatWeight + currentRotSpeed * 0.5f)));
        timer = Mathf.Min(1f, timer);

        float timerSin = Mathf.Sin(timer * Mathf.PI);
        float timerCos = Mathf.Cos(timer * Mathf.PI);
        float timerCos_Hrf = Mathf.Cos((timer * 0.5f) * Mathf.PI);
        float revTimer = (1f - timer);


        float lrValue = (isLeft ? -1f : 1f);



        float minToMin = Mathf.Abs(timer - 0.5f)*2f;
        minToMin = Mathf.Abs(minToMin - 1f);
        minToMin = Mathf.Max(0f, minToMin - 0.25f) * 4f;
        minToMin = Mathf.Min(1f, minToMin * 2f);

        if (!isLeft) LFRWeight = minToMin;
        else RFRWeight = minToMin;


        Transform foot = isLeft ? LFPos : RFPos;


        foot.rotation = Quaternion.Lerp(foot.rotation , Quaternion.LookRotation(target.Forward, target.CurrentCollisionNormal) , timer*0.5f);

        Vector3 rotationBackOfs = (Mathf.Max(timerCos_Hrf,0f) * ( -transform.forward * currentRotSpeed))*0.5f;
        Vector3 rotationRightOfs = timerSin * ((transform.right * lrValue) * currentRotSpeed) * 0.1f;

        Vector3 lrOfs = ((isLeft ? Left : Right) * (footWidth * (1f + (squatWeight * 0.5f))));
        Vector3 pos = targetPos + Vector3.ClampMagnitude((lrOfs + Vector3.ClampMagnitude((Stride * (currentSpeed * 0.3f))/(1f+(currentRotSpeed*0.5f)),0.5f)+(rotationBackOfs + rotationRightOfs)),0.5f);

//        pos += (foot.position - revFoot.position) * (1f-revPow);



        //足の水平座標計算
        foot.position = Vector3.Lerp(startFootPos,LineHit(waist.position,pos),Mathf.SmoothStep(0f,1f,timer));

        //足の高さ計算
        //        float timerHeight = (0.5f - Mathf.Abs(timer - 0.5f));
        float speedHeight = ((currentSpeed * 0.15f) + 0.1f);
        Vector3 upVel = (target.CurrentCollisionNormal * timerSin) * speedHeight;
        foot.position += Vector3.ClampMagnitude(upVel * (((1f - squatWeight) * 0.5f) + 0.5f),0.3f);
        //体の補正
        float loopTimer = (Mathf.Sin((timer + ((isLeft ? 0f : 1f))) * Mathf.PI));

        float speedDrag = Mathf.Max(1f, target.RB.velocity.magnitude - 1f);
        transform.localPosition = new Vector3((loopTimer * 0.03f)/speedDrag,-target.RB.velocity.magnitude*0.03f,0f) + defaPos;
        transform.localEulerAngles = new Vector3(0f, (timerCos * lrValue*20f)/speedDrag, 0f);


        return timer >= 1f;
    }

    void StaticFoot(bool isLeft,float timer)
    {
        Transform foot = isLeft ? LFPos : RFPos;
        foot.position += (target.CurrentCollisionNormal) * ((currentSpeed - 2f) * 0.02f) * (1f - timer);


    }
    // Update is called once per frame
    void FixedUpdate()
    {
        squatWeight = target.SquatWeight;

        Vector3 centerFoot = Vector3.Lerp(LFPos.position, RFPos.position, 0.5f);
        Vector3 movePos = centerFoot+ target.CurrentCollisionNormal * Mathf.Lerp(squatRange.max, squatRange.min, (squatWeight - (currentSpeed * 0.2f)));

        centerObj.position = Vector3.Lerp(centerObj.position, movePos, 0.05f);
        centerObj.position += (target.RB.velocity /target.RB.mass)/2f;

        centerObj.rotation = Quaternion.Lerp(centerObj.rotation, Quaternion.LookRotation(target.Forward,target.CurrentCollisionNormal),0.05f);

        float lrHintOfs = squatWeight*0.5f;
        LFHPos = LFPos.position + ((centerObj.forward * 2f) + ((target.CurrentCollisionNormal*4f + Left) * lrHintOfs));
        RFHPos = RFPos.position + ((centerObj.forward * 2f) + ((target.CurrentCollisionNormal*4f + Right) * lrHintOfs));

        centerObj.Translate(0f,0f,-squatWeight * 0.1f);

        float moveForwardDot = Vector3.Dot(currentVector,transform.forward);

        float currentRotSpeed = Vector3.Angle(transform.forward, prevRot * Vector3.forward) * 0.75f;
        centerObj.Rotate((((squatWeight * 5f) + (squatWeight * moveForwardDot * 1f)) + (moveForwardDot * 1f))-(currentRotSpeed*0.3f),0f,0f);

        //        StaticFoot(!moveFootIsLeft , footTimer);
        if(target.CurrentVector.magnitude > 0f || footTimer > 0f|| Vector3.Distance(centerObj.position, target.Position) > 0.2f || currentRotSpeed > 1f)
        {
            if (MoveFoot(moveFootIsLeft, ref footTimer))
            {
                footTimer = 0f;
                moveFootIsLeft = !moveFootIsLeft;
                startFootPos = moveFootIsLeft ? LFPos.position : RFPos.position;
            }
        }

        prevRot = transform.rotation;

    }
    /// <summary>
    ///物理演算等、すべてが終了した段階でのFixedUpdate
    /// </summary>
    /// <returns></returns>
    IEnumerator LateFixedUpdate()
    {
        while (true)
        {
            yield return new WaitForFixedUpdate();
        }
    }
}
