﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.UI;
//using UnityEngine.Rendering.PostProcessing;


//public class TutorialController : Charactor
//{
//    [SerializeField]
//    AudioClip hitBallSE;
//    [SerializeField]
//    AudioSource rollBallSE;
//    [SerializeField]
//    AudioClip stopBallSE;
//    AudioClip rollBallSE_Defa;

//    [SerializeField]
//    float groundRad = 0.85f;
//    float GroundFlag;
//    public bool isGround
//    {
//        get { return GroundFlag > 0f ? true : false; }
//    }

//    public bool isGroundEx = false;

//    public KeyManagerHub KM;

//    public PhysicMaterial[] PM;
//    float speed = 100f;


//    Vector3 stickVel;


//    public float stickLen;

//    bool HiSpeedFlag = false;
//    float hiSpeedTimer;



//    enum WallingMode
//    {
//        NULL,
//        Left,
//        Right
//    }
//    WallingMode wallingMode;
//    public bool isWalling
//    {
//        get { return wallingMode != WallingMode.NULL ? true : false; }
//    }
//    float ballingWeight = 0f;//どれだけ玉になってるか


//    float JumpFlag = 0f;//ジャンプやステップをしたよフラグ
//    bool exJumpFlag = false;

//    float nextJumpWait = 0f;            //ジャンプした後のウェイトタイム
//    const float nextJumpLimit = 0.3f;   //上記のウェイト基本値

//    float HoverOnTimer = 0f;        //ホバー状態に移行するまでのウェイトタイム



//    Vector3 bumpNormal;//現在接触しているコライダ情報から計算した、およそ反射すべき法線ベクトル


//    bool bumpFlag = false;
//    float stickAccel = 0f;
//    bool dashFlag = false;


//    //地面に立っているか、壁走り中は法線ベクトルを、それ以外の時は垂直ベクトルを返す
//    public Vector3 GetUpNormal()
//    {
//        if (!isGround)
//        {
//            if (isWalling)
//            {
//                return bumpNormal;
//            }
//            else
//            {
//                return Vector3.up;
//            }
//        }
//        else
//        {
//            return bumpNormal;
//            //            return Vector3.up;
//        }
//    }
//    //ホバリング
//    bool Hover()
//    {

//        if (rb.velocity.y < 0f)
//        {
//            rb.AddForce(Vector3.up * 220f);
//            rb.velocity *= 0.95f;
//            return true;
//        }

//        return false;
//    }
//    //ステップ
//    void Step()
//    {
//        StepWait = 1.5f;

//        Vector3 sv = stickVel;
//        sv.y = 0f;
//        sv = sv.normalized;

//        //ステップをした瞬間、その方向への回転を加える
//        Vector3 torqVel = Vector3.zero;
//        torqVel.z = -sv.x;
//        torqVel.x = sv.z;
//        rb.AddTorque(torqVel * 1000f, ForceMode.VelocityChange);

//        dashFlag = false;
//        nextJumpWait = nextJumpLimit;
//        sv.x *= 7f;
//        sv.z *= 7f;
//        rb.velocity = Vector3.ProjectOnPlane(sv, bumpNormal);
//        //        rb.AddForce(Vector3.up * 3000f);


//    }
//    //ジャンプ
//    void Jump()
//    {

//        nextJumpWait = nextJumpLimit;
//        rb.velocity *= 0.9f;
//        //passiveSkill HighJump
//        rb.AddForce(new Vector3(0f, 9000f, 0f));

//    }
//    //壁ジャンプ
//    bool WallKick(bool autoFlag = false)
//    {
//        float d = Vector3.Dot(stickVel, bumpNormal);
//        if (d > 0.7f || autoFlag)
//        {
//            Vector3 wkVel; ;
//            if (autoFlag)
//            {
//                wkVel = new Vector3(prevBumpNormal.x * 3f, 6f, prevBumpNormal.z * 3f);
//            }
//            else
//            {
//                wkVel = new Vector3(bumpNormal.x * 3f, 6f, bumpNormal.z * 3f);
//            }
//            rb.velocity = new Vector3(rb.velocity.x, 0f, rb.velocity.z);
//            rb.velocity += wkVel;

//            wkVel.y = 0f;
//            wkVel = wkVel.normalized;

//            nextJumpWait = nextJumpLimit;
//            isWallKick = true;
//            return true;
//        }
//        return false;
//    }

//    //壁走り
//    void WallingEffect()
//    {
//        //if((FM.prevFootCycle < Mathf.PI && FM.footCycle >= Mathf.PI) || (FM.prevFootCycle > FM.footCycle))
//        //{
//        //    WallingObjsCount++;
//        //    if (WallingObjsCount >= WallingObjs.Length) { WallingObjsCount = 0; }
//        //    WallingObjs[WallingObjsCount].transform.position = transform.position;
//        //    WallingObjs[WallingObjsCount].transform.rotation = Quaternion.LookRotation(bumpNormal);
//        //    WallingObjs[WallingObjsCount].Pop();

//        //}
//    }
//    void Walling()
//    {
//        //移動方向は、壁走りを始めた瞬間のベクトルできまる
//        if (!isWalling)
//        {
//            Quaternion q = Quaternion.LookRotation(bumpNormal, -Vector3.up);
//            Vector3 n = q * rb.velocity;

//            if (n.x > 0.1f)
//            {
//                wallingMode = WallingMode.Left;
//            }
//            else if (n.x < -0.1f)
//            {
//                wallingMode = WallingMode.Right;
//            }
//            if (wallingMode == WallingMode.NULL)
//            {
//                JumpReset();
//                return;
//            }

//        }
//        //壁走り方向にベクトル加算
//        float rad = wallingMode == WallingMode.Left ? -90f : 90f;

//        //壁の法線ベクトルを回転させ、壁走り方向に曲げる
//        Vector3 calNor = bumpNormal.normalized;
//        calNor = Quaternion.Euler(0f, rad, 0f) * calNor;

//        //速度をあげて、リジッドボディーのベクトル加算
//        calNor *= 4.3f;
//        rb.velocity += (calNor - rb.velocity) * 0.03f;
//        //Yベクトルは０にする
//        rb.velocity = new Vector3(rb.velocity.x, 0f, rb.velocity.z);

//        //重力なし
//        rb.useGravity = false;

//    }
//    //ジャンプアクションリセット
//    void JumpReset()
//    {
//        wallingMode = WallingMode.NULL;
//        GroundFlag = 0f;
//        JumpFlag = 0f;
//        rb.useGravity = true;
//    }

//    //ガード
//    void Balling()
//    {

//    }

//    //接触判定を一通り終えた直後、設置判定や諸々がどうなっているかによって挙動を判定する
//    void CollisionOffCheck()
//    {
//        if (!isBumpEx)
//        {
//            hitPosition = transform.position;
//        }

//        //物体から離れる瞬間、ジャンプ待機状態か、壁走りか等だった場合、とりあえずジャンプする
//        if (!GFlag && isWalling)
//        {
//            //もう何にも触れていなかったら
//            if (!isBumpSp)
//            {
//                Debug.Log("自動壁ジャンプ");
//                bumpNormal = Vector3.up;
//                rb.useGravity = true;
//                WallKick(true);
//                JumpReset();
//            }
//        }
//    }





//    protected override void Main()
//    {
//        isWallKick = false;
//        CollisionOffCheck();

//        //膝の曲がり具合計算
//        downOfs += downAccel;
//        downAccel *= 0.8f;
//        downOfs *= 0.8f;

//        //ハジキ入力判定
//        float prevStickLen = stickLen;
//        stickVel = KM.GetStickVectorFromCamera(ref stickLen);
//        stickVel.y = 0f;
//        stickVel = stickVel.normalized * stickLen;
//        //ハイスピード解除
//        if (KM.GetKey(KeyMode.Stay, Key_Hub.L1) ||
//            KM.GetKey(KeyMode.Stay, Key_Hub.R1) ||
//            KM.GetKey(KeyMode.Stay, Key_Hub.L2)
//            )
//        {
//            ResetHiSpeed();
//        }

//        //ステップ中とボール中はハジキ計算しない
//        if (StepWait > 0f || GFlag)
//            {
//                stickAccel = 0f;
//            }
//            else
//            {
//                stickAccel -= Time.deltaTime;
//                stickAccel += stickLen - prevStickLen;
//                stickAccel = Mathf.Clamp(stickAccel, 0f, 1f);
//                if (stickAccel > 0.85f)//ハジキ確立！
//                {
//                    dashFlag = true;
//                }
//                //ダッシュ判定
//                if (stickLen <= 0.1f) { dashFlag = false; }
//            }

//        //初期初期化
//        rb.freezeRotation = false;

//        //パッシブスキルQuickStepを持っていたらステップ連打できる、ステップ中はステップエフェクト出す
//        StepWait -= Time.deltaTime * 2f;
//        StepWait = StepWait < 0f ? 0f : StepWait;

//        //ジャンプウェイト減らす
//        nextJumpWait -= Time.deltaTime;
//        nextJumpWait = nextJumpWait < 0f ? 0f : nextJumpWait;

//        bool prevBalling = GFlag;
//        //玉状態判定
//        GFlag = KM.GetKey(KeyMode.Stay, Key_Hub.L2);

//        //ジャンプボタン押され続けてる時
//        bool JStayFlag = (KM.GetKey(KeyMode.Stay, Key_Hub.R2) || KM.GetKey(KeyMode.Stay, Key_Hub.B));


//        //玉状態になったりもどったりする時ははねる
//        if (prevBalling != GFlag)
//        {
//            if (GFlag)
//            {
//                //武器消す
//                if (W) W.transform.parent.gameObject.SetActive(false);
//                //フィジックスマテリアルを玉用のにする
//                myCollider.material = PM[1];
//            }
//            else
//            {
//                //武器出す
//                if (W) W.transform.parent.gameObject.SetActive(true);
//                //フィジックスマテリアルもとに戻す
//                myCollider.material = PM[0];
//                //ころがり音けす
//                if (rollBallSE)
//                {
//                    if (rollBallSE.isPlaying) rollBallSE.Stop();
//                }
//            }
//            //跳ねる
//            //if (isGroundEx && nextJumpWait <= 0f)
//            //{
//            //    Vector3 adf = Vector3.up;
//            //    rb.AddForce(adf * 6000f);
//            //}
//            nextJumpWait = nextJumpLimit;
//        }

//        //画面の淵暗さ変更したい
//        PostProcessVolume PPV = CameraManager.PPV;

//        if (GFlag)
//        {
//            W?.Reset();
//            JumpReset();//ジャンプリセット
//            //ブレーキ
//            if (JStayFlag)
//            {
//                if (rollBallSE) rollBallSE.clip = stopBallSE;
//                //PM[1].staticFriction = 1.0f;
//                //PM[1].dynamicFriction = 1.0f;
//                rb.freezeRotation = true;
//            }
//            else
//            {
//                if (rollBallSE) rollBallSE.clip = rollBallSE_Defa;
//                //PM[1].staticFriction = 0.0f;
//                //PM[1].dynamicFriction = 0.1f;

//            }
//            //音だす
//            if (rollBallSE)
//            {

//                rollBallSE.volume = Mathf.Clamp(rb.velocity.magnitude * 0.2f, 0.0f, 10f);
//                rollBallSE.pitch += ((Mathf.Clamp(rb.velocity.magnitude * 0.1f, 0.3f, 0.4f)) - rollBallSE.pitch) * Time.deltaTime;

//                //                rollBallSE.pitch += ((Mathf.Clamp(rb.angularVelocity.magnitude * 0.1f, 0.3f, 1f)) - rollBallSE.pitch) * Time.deltaTime;

//                if (isBumpEx)
//                {
//                    if (!rollBallSE.isPlaying) rollBallSE.Play();
//                }
//                else
//                {
//                    if (rollBallSE.isPlaying) rollBallSE.Stop();
//                }
//            }
//            //音籠らせる
//            CameraManager.ALP.cutoffFrequency += (1000f - CameraManager.ALP.cutoffFrequency) * (Time.deltaTime * 30f);
//            //            CameraManager.ALP.lowpassResonanceQ += (2 - CameraManager.ALP.lowpassResonanceQ) * (Time.deltaTime * 30f);
//            //暗くする
//            PPV.weight = Mathf.Clamp(PPV.weight + Time.deltaTime * 3f, 0f, 1.0f);
//            //小さくなる
//            //ballingWeight += Time.deltaTime;
//            //ballingWeight = Mathf.Clamp(ballingWeight, 0f, 1f);
//            //            transform.localScale = Vector3.Lerp(Vector3.one * 0.75f, Vector3.one * 0.5f, Mathf.SmoothStep(-1f,1f,(ballingWeight*0.5f)+0.5f));
//            //フラグリセット
//            ResetFlags();

//            return;//ボール状態は、自分で何の操作もできない
//        }
//        //        rb.constraints = RigidbodyConstraints.FreezeRotationY;

//        //音開かせる
//        CameraManager.ALP.cutoffFrequency += (100000f - CameraManager.ALP.cutoffFrequency) * (Time.deltaTime * 10f);
//        //        CameraManager.ALP.lowpassResonanceQ += (1 - CameraManager.ALP.lowpassResonanceQ) * (Time.deltaTime * 30f);
//        //明るくする（戻すだけ）
//        PPV.weight = Mathf.Clamp(PPV.weight - Time.deltaTime, 0f, 1.0f);

//        //サイズ元に戻す
//        //ballingWeight -= Time.deltaTime*3f;
//        //ballingWeight = Mathf.Clamp(ballingWeight, 0f, 1f);
////        transform.localScale = Vector3.Lerp(Vector3.one * 0.75f, Vector3.one * 0.5f, Mathf.SmoothStep(0f, 1f, ballingWeight));



//        //地面についてたら摩擦ふやす、そうじゃなければ摩擦へらす（壁刷りでジャンプ力へるのやだ）
//        if (isGround && nextJumpWait <= 0f)
//        {
//            PM[0].staticFriction = 1.0f - (stickLen * 0.5f);
//            PM[0].dynamicFriction = 1.0f - (stickLen * 0.5f);
//        }
//        else
//        {
//            PM[0].staticFriction = 0.0f;
//            PM[0].dynamicFriction = 0.0f;
//        }


//        //武器制御
//        W?.Move();


//        //ステップ
//        //if (dashFlag && isGroundEx && StepWait <= 0f)
//        //{
//        //    Step();
//        //    //            JumpReset();
//        //}
//        //ステップ動作中はキー入力ひかえて
//        if (StepWait > 0.3f/*でもステップ終わり際だったら動かしていいよ*/)
//        {
//            stickVel *= 0f;
//        }
//        //スティック０補正
//        if (stickLen < 0.3f && !isStep)
//        {
//            ResetHiSpeed();
//            stickVel *= 0f;
//            //移動入力してないときはふんばる
//            rb.freezeRotation = true;

//        }
//        else
//        {
//            if (KM.GetKey(KeyMode.Enter, Key_Hub.L3))
//            {
//                HiSpeedFlag = true;
//            }
//            stickVel *= 7.0f;
//        }






//        //ジャンプ関連の計算
//        //ボタンを押してから0.1秒の間はジャンプしたいモード
//        JumpFlag -= JumpFlag > 0f ? Time.deltaTime : 0f;

//        //ボタン押された瞬間
//        if (KM.GetKey(KeyMode.Enter, Key_Hub.R2) || KM.GetKey(KeyMode.Enter, Key_Hub.B))
//        {
//            //ジャンプボタンが押されたら、そのフレームから一瞬、ジャンプしたい状態になる
//            JumpFlag = 0.2f;
//        }


//        //ジャンプ系動作の発動計算
//        if (bumpFlag && nextJumpWait <= 0f && !isStep)
//        {
//            if (isGround)//接地状態だった場合
//            {
//                if (JumpFlag > 0f)
//                {
//                    //ジャンプ
//                    Jump();

//                    JumpReset();
//                }
//            }
//            else//壁に触れていた場合
//            {
//                if (dashFlag)
//                {
//                    if (WallKick())
//                    {
//                        JumpReset();
//                    }
//                }
//                if (JStayFlag && !isWallKick)
//                {
//                    //壁走り
//                    Walling();
//                }
//                else
//                {
//                    if (isWalling)
//                    {
//                        JumpReset();
//                        nextJumpWait = nextJumpLimit * 3f;
//                    }
//                }

//            }
//        }







//        //接地していた場合、玉の最大移動ベクトルを制限する
//        if (isGround)
//        {
//            float friction = (0.92f + (StepWait * 0.08f)) * 1.0f;

//            rb.velocity += (new Vector3(rb.velocity.x * friction, rb.velocity.y, rb.velocity.z * friction) - rb.velocity) * Time.deltaTime * 100f;
//        }
//        else//接地していなかった場合、入力による加算ベクトルはなし
//        {
//            //ダッシュフラグを伏せる
//            dashFlag = false;
//            //ベクトル加算もしない、単純な空中移動補正
//            if (!isWalling)
//            {
//                Vector3 arePos = transform.position;
//                arePos += (stickVel * 0.001f);
//                transform.position = arePos;
//            }
//            //ホバリング状態なら多少の移動制御は可能
//            if (!JStayFlag || isWalling)
//            {
//                HoverOnTimer = 0.1f;
//            }
//            //ホバー
//            bool zeroVelFlag = true;
//            //if (JStayFlag && !isWalling)
//            //{
//            //    if (HoverOnTimer <= 0f)
//            //    {
//            //        if (Hover())
//            //        {
//            //            zeroVelFlag = false;
//            //        }
//            //    }
//            //    else
//            //    {
//            //        HoverOnTimer -= Time.deltaTime;
//            //    }
//            //    //                JumpReset();
//            //}
//            if (zeroVelFlag)
//            {
//                stickVel *= 0f;
//            }
//            else
//            {
//                stickVel *= 0.05f;
//            }
//        }


//        //本体のリジッドボディにベクトルを加算する
//        //ハイスピード加速
//        if (HiSpeedFlag)
//        {
//            hiSpeedTimer += Time.deltaTime * 0.3f;
//            hiSpeedTimer = Mathf.Clamp(hiSpeedTimer, 0f, 1.75f);
//        }

//        Vector3 moveVel = stickVel;
//        Vector3 prjVel = Vector3.ProjectOnPlane(moveVel, bumpNormal);
//        if (prjVel.y > 0f)
//        {
//            prjVel.y *= 0.5f;
//        }
//        Vector3 AddVel = prjVel * speed * (HiSpeedFlag ? hiSpeedTimer : 1f);
//        rb.AddForce(AddVel);


//        //キャラクタ向き
//        //            transform.rotation = Quaternion.Lerp(Quaternion.LookRotation(rb.velocity, Vector3.up), transform.rotation, 0.9f);


//        ResetFlags();
//    }

//    private void ResetHiSpeed()
//    {
//        HiSpeedFlag = false;
//        hiSpeedTimer = 1.2f;
//    }

//    void ResetFlags()
//    {
//        bumpFlag = false;

//        isGroundEx = GroundFlag >= 0.1f;
//        GroundFlag -= Time.deltaTime;
//        prevBumpNormal = bumpNormal;
//        prevBumpCount = nowBumpCount;
//        nowBumpCount = 0;
//        maxUpVel = 0f;
//    }



//    private void OnCollisionEnter(Collision collision)
//    {

//        //        nowBumpCount += collision.contacts.Length;
//        foreach (ContactPoint contact in collision.contacts)
//        {
//            float Impact = Vector3.Dot(collision.relativeVelocity, contact.normal);
//            if (hitBallSE && GFlag)
//            {
//                AudioSource.PlayClipAtPoint(hitBallSE, contact.point, Impact * 0.5f);
//            }
//            //接触しているコライダの中に、自立できる角度の平面があった場合、接地フラグを立てる
//            if (contact.normal.y > groundRad)
//            {
//                if (!isGround)
//                {
//                    if(!GFlag)
//                    {
//                        downAccel = Mathf.Lerp(-0.05f, -0.2f, Impact * 0.1f);
//                        rb.velocity -= (rb.velocity * Mathf.Clamp(Impact, 0f, 1f));
//                    }
//                    nextJumpWait = nextJumpLimit * 0.5f;
//                }
//                GroundFlag = 0.1f;
//            }
//        }

//    }
//    private void OnCollisionExit()
//    {
//        //        nowBumpCount--;
//    }

//    float maxUpVel = 0f;
//    private void OnCollisionStay(Collision collision)
//    {

//        Vector3 v = new Vector3(0f, 0f, 0f);
//        nowBumpCount += collision.contacts.Length;

//        //        GroundFlag = false;
//        foreach (ContactPoint contact in collision.contacts)
//        {
//            //if (contact.point.y + 0.01f < transform.position.y && isWalling)
//            //{
//            //    Debug.Log("UnderHit");//斜めの床とかあたるとあがっちゃう
//            //    JumpReset();
//            //    nextJumpWait = nextJumpLimit * 3f;
//            //}
//            if (contact.point.y - 0.01f > transform.position.y && isWalling)
//            {
//                Debug.Log("HiHit");
//                JumpReset();
//                nextJumpWait = nextJumpLimit * 3f;
//            }
//            if (contact.point.y <= transform.position.y + 0.1f)
//            {
//                bumpFlag = true;
//            }

//            float uVel = Vector3.Dot(Vector3.up, contact.normal);
//            if (maxUpVel == 0f || maxUpVel < uVel)
//            {
//                maxUpVel = uVel;
//                v = contact.normal;

//            }
//            hitPosition = contact.point;
//            //接触しているコライダの中に、自立できる角度の平面があった場合、接地フラグを立てる
//            if (contact.normal.y > groundRad)
//            {
//                GroundFlag = 0.1f;
//            }
//        }

//        //ほしいのは法線ベクトルだけなので、正規化し代入
//        bumpNormal = v.normalized;

//    }

//    protected override void Init()
//    {
//        //        rb = GetComponent<Rigidbody>();
//        if (rollBallSE) rollBallSE_Defa = rollBallSE.clip;
//        myCollider = GetComponent<SphereCollider>();
//    }

//}
