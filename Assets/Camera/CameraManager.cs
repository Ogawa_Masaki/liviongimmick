﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class CameraManager : MonoBehaviour
{
    Action CamMove;
    [SerializeField]
    KeyManagerHub keyManagerHub;
    [SerializeField]
    PlayerBall target;
    [SerializeField]
    Rigidbody offsetterRigidbody;
    Transform offsetter => offsetterRigidbody.transform;
    Vector3 defaultOffsetPos;

    [SerializeField]
    float upOfs = 1.1f;
    float hOffset;
    bool ResetFlag = false;



    static AudioLowPassFilter ALP;
    static PostProcessVolume PPV = null;

    Vector3 AimPos;

    [SerializeField]
    float speed = 1f;
    float defaSpeed;


    LayerMask mask;
    Transform lineMaster;
    public Transform LineMaster
    {
        get { return lineMaster; }
        set
        {
            lineMaster = value;
            SetD2Lines();
        }
    }


    List<Transform> D2Lines = new List<Transform>();
    void SetD2Lines()
    {
        D2Lines = new List<Transform>();
        foreach (Transform t in lineMaster)
        {
            D2Lines.Add(t);
        }
    }

    public enum CamMode
    {
        LockPosRad,
        LockPosLookRad,
        D3,
        D2RadLock,
        D2RadLook
    }
    CamMode camMode;

    public CamMode mode
    {
        get { return camMode; }
        set
        {
            camMode = value;
            switch (camMode)
            {
                case CamMode.LockPosRad:
                    {
                        CamMove = Move_LockPos;
                        CamMove += Move_LockRad;
                    }
                    break;
                case CamMode.LockPosLookRad:
                    {
                        CamMove = Move_LockPos;
                        CamMove += Move_Look;
                    }
                    break;
                case CamMode.D3:
                    {
                        CamMove = Move_3D;
                    }
                    break;
                case CamMode.D2RadLock:
                    {
                        CamMove = Move_2D;
                        CamMove += Move_LockRad;
                    }
                    break;
                case CamMode.D2RadLook:
                    {
                        CamMove = Move_2D;
                        CamMove += Move_Look;
                    }
                    break;
                default: { } break;
            }
        }
    }


    // Use this for initialization

    public void Init()
    {
        Camera.main.transform.parent = offsetter;
        Camera.main.transform.localPosition = Vector3.zero;
        Camera.main.transform.rotation = Quaternion.identity;
        transform.parent = null;

        defaultOffsetPos = offsetter.localPosition;

        defaSpeed = speed;
        mode = CamMode.D3;
        if (lineMaster)
        {
            SetD2Lines();
        }
        mask = 1 << LayerMask.NameToLayer("Map");

        defaZoom = Camera.main.fieldOfView;
        ALP = Camera.main.GetComponent<AudioLowPassFilter>();
        PPV = Camera.main.GetComponent<PostProcessVolume>();
//        Camera.main.GetComponent<PostProcessVolume>().profile.TryGetSettings<Vignette>(out vignette);
    }

    void Move_Look()
    {
        offsetter.rotation = Quaternion.Lerp(Quaternion.LookRotation(target.transform.position - offsetter.position) , offsetter.rotation , 0.9f);

        //エイム座標系さん
        Vector3 sVel = keyManagerHub.GetStickVel(false);
        if (sVel.magnitude > 0.1f)
        {
            sVel.z = -sVel.y;
            sVel.y = 0f;
        }
        else
        {
            sVel = Vector3.forward;
        }
        Vector3 cf = Camera.main.transform.forward;
        cf.y = 0f;
        Vector3 sv = Quaternion.LookRotation(sVel) * cf.normalized;
        AimPos = target.transform.position + sv + (Vector3.up * 0.5f);

    }
    void Move_LockPos()
    {
        offsetter.position = lineMaster.transform.position;
    }
    void Move_LockRad()
    {
        offsetter.rotation = Quaternion.Lerp(lineMaster.transform.rotation, offsetter.rotation,0.9f);
        //エイム座標系さん
        Vector3 sVel = keyManagerHub.GetStickVel(false);
        if (sVel.magnitude > 0.1f)
        {
            sVel.y = -sVel.y;
        }
        else
        {
            sVel = Vector3.right;
        }
        Vector3 sv = Quaternion.LookRotation(sVel) * Camera.main.transform.forward;
        AimPos = target.transform.position + sv + (Vector3.up*0.5f);
    }

    void Move_2D()
    {
        List<Transform> lines = D2Lines.OrderBy( b => Vector3.Distance(target.transform.position , b.position)).ToList();

        for (int i = 0; i < D2Lines.Count; i++)
        {
            if(lines[0] == D2Lines[i])
            {
                int a = Mathf.Clamp(i - 1, 0, D2Lines.Count-1);
                int b = Mathf.Clamp(a + 1, 0, D2Lines.Count - 1);
                int c = Mathf.Clamp(b + 1, 0, D2Lines.Count - 1);

                Vector3 abPos = Mathaf.NearPosOnLine(target.transform.position, D2Lines[a].position, D2Lines[b].position);
                Vector3 bcPos = Mathaf.NearPosOnLine(target.transform.position, D2Lines[b].position, D2Lines[c].position);

                float abLen = Vector3.Distance(target.transform.position, abPos);
                float bcLen = Vector3.Distance(target.transform.position ,bcPos);
                Vector3 nPos = abLen > bcLen ? bcPos : abPos;
                offsetter.position += (nPos - offsetter.position) * Time.deltaTime;
                break;
            }

        }

    }

    public Transform Scope;
    bool zoomFlag;
    float zoomValue = 1f;
    float nowZoomValue = 1f;
    Vector3 rotateVector;
    public void SetZoom(bool flag, float value)
    {
        zoomFlag = flag;
        zoomValue = value;
    }
    public void ResetZoom()
    {
        zoomFlag = false;
    }
    public float defaZoom;
     float targetExWeight = 0f;

    public float AimRand
    {
        get { return 1f -targetExWeight; }
    }
    void Move_3D()
    {
        if (ResetFlag) { speed = 1.0f; }
        else
        {
            speed = defaSpeed;
        }

        Vector3 gyro = keyManagerHub.GetGyroVel(false);
        Vector3 stickVel = keyManagerHub.GetStickVel(false);
        rotateVector += (stickVel - rotateVector) * Time.deltaTime*8f;

        if (!UnityEngine.XR.XRDevice.isPresent)
        {
            //子 カメラ X軸 移動、VRでこれやったら死ぬ、死んだ
            if (keyManagerHub.hard == KeyManagerHub.HardMode.Switch)
            {
                offsetter.Rotate(gyro.x * speed, 0f, 0f);
            }
            else
            {
                offsetter.Rotate(rotateVector.y * speed, 0f, 0f);
            }


        }

        //カメラ移動
        float zoomLen = defaultOffsetPos.z;

        Vector3 toPos = Mathaf.NearPosOnLine(transform.position,
            target.transform.position - offsetter.transform.right*2f,
            target.transform.position + offsetter.transform.right*2f);
        toPos = Vector3.Lerp(target.transform.position, toPos, 1f-Time.deltaTime);


        transform.position = toPos;

        hOffset += ((upOfs + (-zoomLen * 0.1f)) - hOffset) * 0.5f;
        toPos += (hOffset + (ResetFlag ? -0.5f : 0f))*target.CurrentCollisionNormal;
        toPos += transform.forward * zoomLen;//子カメラの目標座標を後ろにずらす

        offsetter.transform.position += (toPos - offsetter.transform.position) * Time.deltaTime;


        //スコープのぞき込み
        targetExWeight += Time.deltaTime * (zoomFlag ? 5f : -5f);
        targetExWeight = Mathf.Clamp(targetExWeight, 0f, 1f);
        nowZoomValue += (zoomValue - nowZoomValue) * (Time.deltaTime*10f);
        Camera.main.fieldOfView = Mathf.Lerp(defaZoom, defaZoom * nowZoomValue, targetExWeight);
        if (Scope)
        {
            offsetter.transform.position = Vector3.Lerp(offsetter.transform.position, Scope.position, targetExWeight);
        }

        //親カメラ回転
        transform.Rotate(0f, (gyro.y + rotateVector.x) * speed, 0f);
        //子カメラリセット
        //if (keyManagerHub.GetKey(Flag.Mode.Enter, Key_Hub.R3) || ResetFlag)
        //{
        //    offsetter.localRotation = Quaternion.Euler(new Vector3(10f, 0f, 0f));
        //}
        //子カメラ回転
        offsetter.rotation = Quaternion.Euler(offsetter.transform.eulerAngles.x, transform.eulerAngles.y, 0f);

        //子カメラ移動補正
        float viewOfs = Camera.main.WorldToScreenPoint(target.transform.position).y- (Screen.height/2f);

        bool u = viewOfs > 0f ? true : false;
        viewOfs = Mathf.Abs(viewOfs);
        viewOfs -= Screen.height/3f;
        if (viewOfs < 0f) viewOfs = 0f;
        viewOfs *= u ? 1f : -1f;

        offsetter.position += offsetter.up * (viewOfs / (Screen.height*3f));

        //エイム座標系さん
        AimPos = Camera.main.transform.position + (Camera.main.transform.forward*10f);

        //リコイル
        recoilTime -= Time.deltaTime;
        if(recoilTime <= 0f)
        {
            recoilInvTime -= Time.deltaTime;
        }
        Vector3 rVel = Vector3.Lerp(Vector3.zero, recoilVel, recoilTime * 10f);
        Vector3 rMem = Vector3.Lerp(Vector3.zero, recoilMem, recoilInvTime);

        recoilTime = Mathf.Clamp(recoilTime, 0f, 1f);
        recoilInvTime = Mathf.Clamp(recoilInvTime, 0f, 1f);
        offsetter.Rotate(rVel.x- rMem.x, 0f, 0f);
        transform.Rotate(0f, rVel.y- rMem.y, 0f);

        recoilInv += Time.deltaTime;
        recoilInv = Mathf.Clamp(recoilInv, 0.1f, 1f);


        //Vector3 screenCenterPos = new Vector3(Screen.width * 0.5f, Screen.height * 0.5f, 0f);
        //Ray ray = Camera.main.ScreenPointToRay(screenCenterPos);
        //RaycastHit hit = new RaycastHit();
        //if (Physics.Raycast(ray,out hit,float.MaxValue,mask))
        //{
        //    AimPos = hit.point;
        //}

    }
    Vector3 recoilMem = Vector3.zero;
    Vector3 recoilVel = Vector3.zero;
    float recoilTime = 0f;
    float recoilInv = 0f;
    float recoilInvTime = 0f;
    const float minRecoil = 0.3f;

    public void Recoil(float h , float l, float r , int RecoilPow)
    {
        recoilInv -= 1f/RecoilPow;
        recoilInv = Mathf.Clamp(recoilInv, 0.1f, 1f);
        recoilVel.x = h*recoilInv;
        recoilVel.y = UnityEngine.Random.Range(-l * (recoilInv+minRecoil), r * (recoilInv + minRecoil));
        recoilMem += recoilVel*0.1f;
        recoilMem = recoilMem*0.5f* recoilInv;
        recoilInvTime = 1f;
        recoilTime = 0.1f;
    }

    // Update is called once per frame
    void Update()
    {
        CamMove?.Invoke();
        offsetterRigidbody.velocity *= 0.5f;

    }
}
